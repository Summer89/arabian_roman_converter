
#!/usr/bin/python
# -*- coding: utf-8 -*-

# Прогама конвертує цілі числа в арабському запису в римську форму запису в межах від 1 до 1000000 включно.
# І навпаки

ROMAN_EQUIVALENCES = {
       1: 'I', 4: 'IV', 5: "V", 9: "IX",
       10: "X", 40: "XL", 50: "L", 90: "XC",
       100: "C", 400: "CD", 500: "D", 900: "CM",
       1000: "M", 4000:'MV\u0305', 5000:'V\u0305', 9000:'MX\u0305',
       10000:'X\u0305', 40000:'X\u0305L\u0305', 50000:'L\u0305', 90000:'X\u0305C\u0305',
       100000:'C\u0305', 400000:'C\u0305D\u0305', 500000:'D\u0305', 900000:'C\u0305M\u0305',
       1000000:'M\u0305', 4000000:'M\u0305V\u033F', 5000000:'V\u033F', 9000000:'M\u0305X\u033F',
       10000000:'X\u033F', 40000000:'X\u033FL\u033F', 50000000:'L\u033F', 90000000:'X\u033FC\u033F',
       100000000:'C\u033F', 400000000:'C\u033FD\u033F', 500000000:'D\u033F'
        }

ROMAN_VALUES = {
        'I':1, 'V':5, 'X':10, 'L':50,
        'C':100, 'D':500, 'M':1000,
        'V\u0305':5000, 'X\u0305':10000, 'L\u0305':50000,
        'C\u0305':100000, 'D\u0305':500000, 'M\u0305':1000000,
        'V\u033F':5000000, 'X\u033F':10000000, 'L\u033F':50000000,
        'C\u033F':100000000, 'D\u033F':500000000
        }

def convert(number):
    result = ''
    if number == 0:
        return "\u2015"
    operands = sorted(ROMAN_EQUIVALENCES.keys(), reverse=True)
    for integer in operands:
        while number >= integer:
            result += ROMAN_EQUIVALENCES[integer]
            number -= integer
    return result

def convert_roman_to_arabian(roman_integer):
    integer = 0
    old_value = None
    old_token = ''
    for token in reversed(roman_integer):
        if token == '\u0305' or token == '\u033F':
            old_token += token
            continue
        token += old_token
        new_value = ROMAN_VALUES[token]
        integer -= new_value if old_value and old_value > new_value else (-new_value)
        old_value = new_value
        old_token = ''
    return integer



