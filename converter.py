#!/usr/bin/python
# -*- coding: utf-8 -*-


from bottle import *
import arabian_roman
import re


PREFIX = ''


app = application = Bottle()


@app.route(PREFIX + '/converter/css/<filename>')
def server_static(filename):
    return static_file(filename, root='./css')


@app.route(PREFIX + '/converter/')
@view('page')
def start_page(value="", roman="Немає даних"):
    return dict(value=value, roman=roman, prefix=PREFIX)


@app.post(PREFIX + '/converter/')
@view('page')
def convert():
    roman = "Немає даних"
    value = request.forms.getunicode("value").strip().replace(" ", "")
    value += request.forms.getunicode("converting_value").strip().replace(" ", "")\
        if request.forms.get("converting_value") else ""
    if request.forms.get("converting"):
        if value.isdecimal():
            if 899999999 >= int(value):
                roman = arabian_roman.convert(int(value))
            elif int(value) >= 899999999:
                roman = "Число вийшло за допустимий діапазон"
        elif re.match(r'^[IVXLCDM\u0305\u033F]+$', value):
            preview = arabian_roman.convert_roman_to_arabian(value)
            control = arabian_roman.convert(int(preview))
            roman = preview if control == value else 'Число введено не за правилами написання римських чисел'
        else:
            roman = "Невірно введено число"
        return dict(value=value, roman=roman, prefix=PREFIX)
    elif request.forms.get("reset"):
        return dict(value='', roman='Немає даних', prefix=PREFIX)
    else:
        return dict(value=value, roman='Немає даних', prefix=PREFIX)

if __name__ == '__main__':
    run(app=app, host='localhost', port=8080)