<html>
  <head>
    <title>Конвертeр римських та арабських чисел</title>
    <link rel="stylesheet" type="text/css" href="{{prefix}}/converter/css/style.css" />
    <meta http-equiv="Content-Type"
    content="text/html"; charset="UTF-8" />
    <meta name="keywords" content="конвертер, римские, преобразование, арабские, числа, римські, перетворення, арабські, числа, convert, arabian, roman, converter, numbers">
    <meta http-equiv="DESCRIPTION"
    content="Перевод римських чисел в арабські та навпаки." />
  </head>
  <body bgcolor="khaki" leftmargin="50" topmargin="20"
        rightmargin="50" bottommargin="20" text="darkslategray"
        alink="red" vlink="green">
      <font color="brown"><h1 align="center">Конвертер чисел:</h1></font><br />
        Для коректного переводу чисел
        необхідно опрацювати теоретичні
        викладки, які ви знайдете тут
      <a href="https://ru.wikipedia.org/wiki/Римские_цифры"
        target="_blank"><em>Римські числа:</em></a> <br />
        Програма працює з числами в діапазоні від 0 до 899999999 включно.<br /><br />
      <form method='post' action='{{prefix}}/converter/' accept-charset="UTF-8">
<table bgcolor="khaki" >
	   <tr><td align="center" valign="middle" width="400">
<input type="submit" name="converting" value="ПЕРЕВЕСТИ" style="width:150Px;height:40Px "/><br /><br />
<input type="submit" name="reset" value="ОЧИСТИТИ ПОЛЕ" style="width:150Px;height:40Px "/><br /><br />
       <td/><td>
	<table width="80" bgcolor="khaki" >
	   <tr><td>1</td><td>
<input type="submit" name="converting_value" value="I" style="width:40Px;height:40Px" />
	   </td></tr>
	   <tr><td>5</td><td>
<input type="submit" name="converting_value" value="V" style="width:40Px;height:40Px" />
	   </td></tr>
	   <tr><td>10</td><td>
<input type="submit" name="converting_value" value="X" style="width:40Px;height:40Px" />
	   </td></tr>
	   <tr><td>50</td><td>
<input type="submit" name="converting_value" value="L" style="width:40Px;height:40Px" />
	   </td></tr>
	   <tr><td>100</td><td>
<input type="submit" name="converting_value" value="C" style="width:40Px;height:40Px" />
	   </td></tr>
	   <tr><td>500</td><td>
<input type="submit" name="converting_value" value="D" style="width:40Px;height:40Px" />
	   </td></tr>
    	</table>
	   </td><td>
	<table width="80" bgcolor="khaki" >
	   <tr><td>1000</td><td>
<input type="submit" name="converting_value" value="M" style="width:40Px;height:40Px" />
	   </td></tr>
	   <tr><td>5000</td><td>
<input type="submit" name="converting_value" value="V&#x305" style="width:40Px;height:40Px" />
	   </td></tr>
	   <tr><td>10000</td><td>
<input type="submit" name="converting_value" value="X&#x305" style="width:40Px;height:40Px" />
	   </td></tr>
	   <tr><td>50000</td><td>
<input type="submit" name="converting_value" value="L&#x305" style="width:40Px;height:40Px" />
	   </td></tr>
	   <tr><td>100000</td><td>
<input type="submit" name="converting_value" value="C&#x305" style="width:40Px;height:40Px" />
	   </td></tr>
	   <tr><td>500000</td><td>
<input type="submit" name="converting_value" value="D&#x305" style="width:40Px;height:40Px" />
	   </td></tr>
    	</table>
	   </td><td>
	<table width="80" bgcolor="khaki" >
	   <tr><td>1000000</td><td>
<input type="submit" name="converting_value" value="M&#x305" style="width:40Px;height:40Px" />
	   </td></tr>
	   <tr><td>5000000</td><td>
<input type="submit" name="converting_value" value="V&#x33F" style="width:40Px;height:40Px" />
	   </td></tr>
	   <tr><td>10000000</td><td>
<input type="submit" name="converting_value" value="X&#x33F" style="width:40Px;height:40Px" />
	   </td></tr>
	   <tr><td>50000000</td><td>
<input type="submit" name="converting_value" value="L&#x33F" style="width:40Px;height:40Px" />
	   </td></tr>
	   <tr><td>100000000</td><td>
<input type="submit" name="converting_value" value="C&#x33F" style="width:40Px;height:40Px" />
	   </td></tr>
	   <tr><td>500000000</td><td>
<input type="submit" name="converting_value" value="D&#x33F" style="width:40Px;height:40Px" />
	   </td></tr>
    	</table>
	   </td></tr>
</table>
<br /><br />

        <input type="text" name="value" size="60"
               maxlength="60" placeholder="Введіть дані" value="{{value}}" /><br /><br />
        <font color="brown"><h2>{{roman}}</h2></font>
         <div id="copyright">&copy; Bezobiuk Igor 2015</div>


      </form>

  </body>
</html>
